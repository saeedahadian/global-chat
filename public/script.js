//----------
//  Socket
//----------
const socket = io(); // eslint-disable-line no-undef

//--------------------
//  Global variables
//--------------------
/**
 * Define an object which holds all global vairables that we're going to
 * need in this code.
 */
const globalVars = { myUsername: null };

//-----------
//  Sign-in
//-----------
const SIGNIN_FORM = document.getElementById('signin');

SIGNIN_FORM.addEventListener('submit', event => {
  // Prevent form from refreshing the page on submit.
  event.preventDefault();

  /**
   * Get person's username and store it in a "global" variable for later
   * user.
   */
  const USERNAME_INPUT = document.getElementById('username').value;
  if (USERNAME_INPUT) {
    // If username input is not empty, store it.
    globalVars.myUsername = USERNAME_INPUT;

    // And then show the chat page.
    const SIGNIN_CONTAINER = document.getElementById('signin-container');
    const CHAT_CONTAINER = document.getElementById('chat-container');

    SIGNIN_CONTAINER.classList.add('hidden');
    CHAT_CONTAINER.classList.remove('hidden');

    /**
     * Send the username to the server for showing this user among the
     * online people.
     */
    socket.emit('signin', { username: globalVars.myUsername });
  } else {
    // TODO: Show error message.
  }
});

//-----------
//  Chatbox
//-----------
const CHATBOX_FORM = document.getElementById('chat-box');

CHATBOX_FORM.addEventListener('submit', event => {
  // Prevent form from refreshing the page on submit.
  event.preventDefault();

  /**
   * Send the message for everyone including yourself. User cannot send
   * an empty message.
   */
  const MSG_INPUT = document.getElementById('chat-message');
  if (MSG_INPUT.value) {
    // If user provided a message, send it.
    socket.emit('message', {
      username: globalVars.myUsername,
      message: MSG_INPUT.value,
    });

    // Then empty out the input.
    MSG_INPUT.value = '';
  } else {
    /**
     * TODO: Show an error or an effect that indicates the message is
     * empty.
     */
  }
});

/**
 * Get an online user and show it in the list of online people.
 */
socket.on('online-users', onlineUsers => {
  // Select online people's div.
  const onlinePeopleDiv = document.getElementById('chat-online-people');

  // Clear list for a fresh start.
  while (onlinePeopleDiv.firstChild) {
    onlinePeopleDiv.firstChild.remove();
  }

  for (const userId in onlineUsers) {
    // Select the username.
    const username = onlineUsers[userId];

    // Create a text node of the user.
    const usernameTextNode = document.createTextNode(username);
    const usernamePara = document.createElement('P');

    // Update it with new data.
    usernamePara.append(usernameTextNode);
    onlinePeopleDiv.append(usernamePara);
  }
});

socket.on('user-disconnected', onlineUsers => {
  // Select online people's div.
  const onlinePeopleDiv = document.getElementById('chat-online-people');

  // Clear list for a fresh start.
  while (onlinePeopleDiv.firstChild) {
    onlinePeopleDiv.firstChild.remove();
  }

  for (const userId in onlineUsers) {
    // Select the username.
    const username = onlineUsers[userId];

    // Create a text node of the user.
    const usernameTextNode = document.createTextNode(username);
    const usernamePara = document.createElement('P');

    // Update it with new data.
    usernamePara.append(usernameTextNode);
    onlinePeopleDiv.append(usernamePara);
  }
});

/**
 * Show the message on the chatbox.
 */
socket.on('own-message', data => {
  const CHAT_HISTORY = document.getElementById('chat-history');

  // Create text nodes.
  const { username, message } = data;
  const usernameTextNode = document.createTextNode(username);
  const messageTextNode = document.createTextNode(message);

  // Create paragraph elements for these text nodes.
  const usernamePar = document.createElement('P');
  const usernameSmall = document.createElement('SMALL');
  const messagePar = document.createElement('P');

  usernamePar.append(usernameSmall);
  usernameSmall.append(usernameTextNode);
  messagePar.append(messageTextNode);

  // Add styling classes to paragraphs.
  usernamePar.classList.add('self-end');
  messagePar.classList.add('self-end');

  /**
   * Append to chat history.
   */
  CHAT_HISTORY.append(usernamePar, messagePar);

  /**
   * Then, scroll to the bottom of chat history as new messages get
   * added to it.
   */
  CHAT_HISTORY.scrollTo({
    top: CHAT_HISTORY.scrollHeight,
    behavior: 'smooth',
  });
});

socket.on('others-message', data => {
  const CHAT_HISTORY = document.getElementById('chat-history');

  // Create text nodes.
  const { username, message } = data;
  const usernameTextNode = document.createTextNode(username);
  const messageTextNode = document.createTextNode(message);

  // Create paragraph elements for these text nodes.
  const usernamePar = document.createElement('P');
  const usernameSmall = document.createElement('SMALL');
  const messagePar = document.createElement('P');

  usernamePar.append(usernameSmall);
  usernameSmall.append(usernameTextNode);
  messagePar.append(messageTextNode);

  // Add styling classes to paragraphs.
  usernamePar.classList.add('text-purple-600');
  messagePar.classList.add('text-purple-600');

  /**
   * Append to chat history.
   */
  CHAT_HISTORY.append(usernamePar, messagePar);

  /**
   * Then, scroll to the bottom of chat history as new messages get
   * added to it.
   */
  CHAT_HISTORY.scrollTo({
    top: CHAT_HISTORY.scrollHeight,
    behavior: 'smooth',
  });
});

/**
 * TODO: When user's typing, broadcast it to others.
const CHAT_MSG = document.getElementById('chat-message');

CHAT_MSG.addEventListener('input', event => {
  // Emit "isTyping" event.
  socket.emit('isTyping', { username });

  socket.once('isTyping', data => {
    const CHAT_TYPING = document.getElementById('chat-typing');

    console.log(data);
    let isTypingMsg = `${data.username} is typing...`;
    let isTypingNode = document.createTextNode(isTypingMsg);
    CHAT_TYPING.append(isTypingNode);
  });
});
 */
