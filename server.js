//-----------
//  Modules
//-----------
// Node modules
const path = require('path');

// Express server
const express = require('express');

const app = express();
const server = require('http').createServer(app);

// Socket server
const socketIo = require('socket.io');

const io = socketIo(server);

//-----------------
//  Online People
//-----------------
/**
 * Using an object to store a list of online users with their socket
 * id.
 */
const onlineUsers = {};

//-------------
//  IO Events
//-------------
io.on('connection', socket => {
  console.log('A user connected with socket id: ', socket.id);

  socket.on('signin', data => {
    const { username } = data;

    // Add the username and its socket id to the object.
    onlineUsers[socket.id] = username;

    io.emit('online-users', onlineUsers);
  });

  //------------------
  //  Message Events
  //------------------
  /**
   * To be able to distinguish a user's messages from others, we need
   * to define 2 separate events:
   *
   * 1. 'own-message' event: Only emits user's messages back to
   *    themselves.
   * 2. 'others-message' event: Only emits user's message to others.
   */
  socket.on('message', data => {
    const { username, message } = data;

    // For user's own messages,
    socket.emit('own-message', { username, message });

    // For others' messages
    socket.broadcast.emit('others-message', { username, message });
  });

  /**
   * TODO: isTyping feature.
  socket.on('isTyping', data => {
    // Broadcast the user who's typing a message!
    let username = data.username;
    socket.broadcast.emit('isTyping', { username });
  });
  */

  socket.on('disconnect', () => {
    console.log(`User with socket id ${socket.id} disconnected.`);

    /**
     * Update your object of online users and delete the disconnected
     * user from it, then send the disconnected user's information back
     * to the client.
     */
    delete onlineUsers[socket.id];
    io.emit('user-disconnected', onlineUsers);
  });
});

//-----------
//  Routes
//-----------
app.use('/static', express.static('./public'));
app.get('/', (req, res) => {
  res.sendFile(path.resolve(__dirname, './public/index.html'));
});

//-------------------------------
//  Create server on port 3000.
//-------------------------------
const API_PORT = process.env.PORT || 3000;
server.listen(API_PORT, () => console.log('Up and running on port 3000...'));
